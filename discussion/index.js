


//placeholder database: https://jsonplaceholder.typicode.com/posts
// fetch - used to perform crud operations in a given url
	// accepts 2 arguments - url (needed) & options (not a requirement)
	// options paraneter - used only when the dev needs the request body from the user



// get the post data
/*
	mini activity - use fetch method to get the post inside the "https://jsonplaceholder.typicode.com/posts"
				- make the response in JSON format (.then)
				- log the response in the console (.then)
*/	


fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then (data => showPost(data))




// add post
document.querySelector("#form-add-post").addEventListener(`submit`, (e) => {
	e.preventDefault();

	fetch(`https://jsonplaceholder.typicode.com/posts`, {
		method: "POST",
		body: JSON.stringify({
			title:document.querySelector("#txt-title").value,
			body:document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers:{"Content-Type": "application/json; charset=UTF-8"}
	})
.then((response) => response.json()) //converts the response into JSON format
.then (data => {
	//showPost([data]);//only shows the newly added the data
	//showPost(data)
	alert("Post Created Successfuly")

	// to clear the text in the input fields upon creating a post
	document.querySelector(`#txt-title`).value = null;
	document.querySelector(`#txt-body`).value = null;
	})
})

/*
	create a showPost function that will display the posts in the placeholder database as well as the posts created in the webpage

		the posts should appear in the div under the "Posts"

		send a screenshot of the output in the batch hangouts/google
*/


const showPost = (posts) =>{
	let postEntries = [];

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	document.querySelector("#div-post-entries").innerHTML = postEntries;
}



//edit post function

const editPost= (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value=id;
	document.querySelector('#txt-edit-title').value=title;
	document.querySelector('#txt-edit-body').value=body;

	//remove the attribute from the element
	document.querySelector("#btn-submit-update").removeAttribute('disabled');
}


// update post
document.querySelector(`#form-edit-post`).addEventListener('submit', (e) =>{
	e.preventDefault()
	fetch(`https://jsonplaceholder.typicode.com/posts/1`, {
	method:"PUT",
	body:JSON.stringify({
		id:document.querySelector("#txt-edit-id").value,
		title:document.querySelector("#txt-edit-title").value,
		body:document.querySelector("#txt-edit-body").value,
		userId:1
	}),
	headers: {"Content-Type": "application/json;charset=UTF-8"}
 })
	.then(response => response.json())
	.then(data => {
		console.log(data);
		alert('Post Successfuly Updated')

	// to clear the text in the input fields upon creating a post
	document.querySelector(`#txt-edit-id`).value = null;
	document.querySelector(`#txt-edit-title`).value = null;		
	document.querySelector(`#txt-edit-body`).value = null;	


	// set attribute - sets the attribute of the element; accepts two arguments
		// string - the attribute to be set
		// boolean - to be set into true/false
	document.querySelector("#btn-submit-update").setAttribute('disabled', true);	
	})
})


/*
	ACTIVITY: make the "Delete" button work
		only the post which delete button is pressed should be deleted

	hint: could be a 3-line code
*/


const deletePost = (id) => {
	fetch (`https://jsonplaceholder.typicode.com/posts/${id}`, {method: "DELETE"});
	document.querySelector(`#post-${id}`).remove()
}














